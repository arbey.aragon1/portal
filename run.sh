sudo docker kill $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)

sudo docker build -t prj:1 .
sudo docker run -it -d \
    --name project\
    -p 4200:4200 \
    --cpus 2.0 --memory 3g --memory-swap 3g \
    -v $(pwd)/project:/project \
    prj:1 bash
    
sudo docker exec -it project bash
