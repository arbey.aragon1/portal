FROM node:latest

RUN npm install -g typescript
RUN npm install -g ts-node 
RUN npm install -g @angular/cli

EXPOSE 4200
CMD bash
