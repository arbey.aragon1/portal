import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  data: any = {};
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAllUsers()
      .pipe(
        tap((val) => {
          this.data = val;
        })
      )
      .subscribe();
  }

}
