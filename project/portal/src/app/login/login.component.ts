import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { take, tap } from 'rxjs/operators';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public logInForm: FormGroup;
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.logIn();
  }

  public logInWithEmailAndPassword(): void {
    this.authService.signInWithEmailAndPassword(
      this.logInForm.get('email').value,
      this.logInForm.get('password').value
    ).pipe(
      tap(() => {
        this.router.navigate(['/dashboard']);
      })
    )
      .subscribe();
  }

  private logIn(): void {
    this.logInForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    });
  }


}
