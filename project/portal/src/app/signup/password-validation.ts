import { AbstractControl } from '@angular/forms';

export function matchPassword(formControl: AbstractControl) {

    const password: string = formControl.get('password').value;
    const confirmPassword: string = formControl.get('confirmedPassword').value;

    if (password !== confirmPassword) {
        formControl.get('confirmedPassword')
            .setErrors({ matchPassword: true });
    } else {
        return null;
    }
}