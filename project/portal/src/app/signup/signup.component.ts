import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { take, tap } from 'rxjs/operators';
import { matchPassword } from './password-validation';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public registerForm: FormGroup;
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private userService: UserService) { }

  ngOnInit() {
    this.createRegisterForm();
  }

  public signUp(): void {
    let user: any = {
      'fullName': this.registerForm.get('name').value + ' ' + this.registerForm.get('familyName').value,
      'email': this.registerForm.get('email').value,
    };
    this.authService.signUp(
      this.registerForm.get('name').value + ' ' + this.registerForm.get('familyName').value,
      this.registerForm.get('email').value,
      this.registerForm.get('password').value
    ).pipe(tap((val) => {
      this.userService.addUser(user);
    }));
  }

  private createRegisterForm(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', [
        Validators.required
      ]],
      familyName: ['', [
        Validators.required
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]],
      confirmedPassword: ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    }, {
      validator: matchPassword
    });
  }
}
