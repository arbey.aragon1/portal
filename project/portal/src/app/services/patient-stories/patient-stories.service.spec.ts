import { TestBed } from '@angular/core/testing';

import { PatientStoriesService } from './patient-stories.service';

describe('PatientStoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PatientStoriesService = TestBed.get(PatientStoriesService);
    expect(service).toBeTruthy();
  });
});
