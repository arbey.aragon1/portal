import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { from, Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static readonly DBG: boolean = true;

  private _isLoggedIn: boolean = false;
  private _currentUser: any;
  private _subUser: any;
  private _statusChanges: number;

  private _user: BehaviorSubject<any> = new BehaviorSubject<any>(
    undefined
  );

  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  get currentUser(): any {
    return this._currentUser;
  }

  set isLoggedIn(val: boolean) {
    this._isLoggedIn = val;
  }

  set currentUser(user: any) {
    this._currentUser = user;
  }

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router, ) { }

  public signInWithGoogle(): Observable<any> {
    return from(
      this.afAuth.auth
        .signInWithPopup(
          new auth.GoogleAuthProvider()
        )
    );
  }

  public signInWithEmailAndPassword(
    email: string,
    password: string
  ): Observable<any> {
    return from(this.afAuth.auth
      .signInWithEmailAndPassword(
        email,
        password
      )
    );
  }


  public signUp(
    fullName: string,
    email: string,
    password: string
  ): Observable<any> {
    return from(this.afAuth.auth
      .createUserWithEmailAndPassword(
        email,
        password
      ));
  }

  public logout(): Observable<any> {
    return from(this.afAuth.auth.signOut());
  }

  private resetUser(): void {
    this._user.next(undefined);
    this.router.navigate(['/']);
  }

}
