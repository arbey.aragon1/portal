import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePatientStoryComponent } from './create-patient-story.component';

describe('CreatePatientStoryComponent', () => {
  let component: CreatePatientStoryComponent;
  let fixture: ComponentFixture<CreatePatientStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreatePatientStoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePatientStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
