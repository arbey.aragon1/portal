// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCMl7DDh7xRO3dEk1s_hD3vAlg4pU33-EY",
    authDomain: "newagent-a2c77.firebaseapp.com",
    databaseURL: "https://newagent-a2c77.firebaseio.com",
    projectId: "newagent-a2c77",
    storageBucket: "newagent-a2c77.appspot.com",
    messagingSenderId: "721919000634",
    appId: "1:721919000634:web:95688f6c2beafc18f87503"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
